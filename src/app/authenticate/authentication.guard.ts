import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { ThrowStmt } from '@angular/compiler';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private router: Router, private authentificationService: AuthenticationService) {     
    }

    /*canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authentificationService.userAuthenticated()) {
            return true
        } else {
            this.router.navigate(['/signin']);
        } 
    }*/

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Promise<boolean | UrlTree> | Observable<boolean | UrlTree> {
        return this.authentificationService.user.pipe(take(1), map(user => {
           const isAuth = !!user
           if (isAuth) {
               return true;
           } return this.router.createUrlTree(['/signin']);
        }))

    }


}