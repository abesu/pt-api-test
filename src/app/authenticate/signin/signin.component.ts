import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  hide = true;
  failure: string = null;


  constructor(private authenticationService: AuthenticationService, private router: Router, private snackbar: MatSnackBar) { }

  ngOnInit(): void {
  }

  onSignin(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.authenticationService.signIn({
      email: form.value.email,
      password: form.value.password
    }).subscribe(responseValues => {
      console.log(responseValues);
      //this.router.navigate(['/appointments']);
    },
    failureMsg => {
      console.log(failureMsg);
      this.failure = failureMsg;
    });
    //form.reset();
  }

  clearError(){
    this.failure = null;
  }

}
