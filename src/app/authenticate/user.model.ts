export class User {
    constructor(public id: number, private _token: string, private _expirationDate: Date) {

    }
    get token() {
        if (!this._expirationDate || new Date() > this._expirationDate) {
            return null;
        }
        return this._token;
    }

};

export class AuthMessage {
    constructor(public message: string) {
        }
    get resMessage() {
        return this.message;
    }

    }