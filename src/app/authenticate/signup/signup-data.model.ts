export interface SignupData {
    email: string;
    password: string;
    name: string;
    last_name: string;
    num_telephone: string;
    type_user: string;
    password_confirmation: string;
    cgu: string;
    pays: string;
}