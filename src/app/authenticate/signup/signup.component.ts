import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';

/*interface Country {
  value: string;
}*/

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  hide = true;
  failure: string = null;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  /*countries: Country[] = [
    {value: 'Algérie',},
    {value: 'France'}
  ];*/

  onCheck(event: any) {
    event.checked ? event.source.value = 1 : event.source.value = 0;
    console.log(event);
  }

  onRegister(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.authenticationService.signUp({
      email: form.value.email,
      password: form.value.password,
      name: form.value.name,
      last_name: form.value.last_name,
      num_telephone: form.value.num_telephone,
      type_user: form.value.type_user,
      password_confirmation: form.value.password,
      cgu: form.value.cgu,
      pays: form.value.pays,
    }).subscribe(responseValues => {
      console.log(responseValues);
    },
    failureMsg => {
      console.log(failureMsg);
      this.failure = failureMsg;
    });
    //form.reset();
  }

  clearError(){
    this.failure = null;
  }

}