import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.css']
})
export class ValidateComponent implements OnInit {
  hide = true;
  failure: string = null;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
  }

  onValidate(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.authenticationService.validateAccount({
      //email: form.value.email,
      code: form.value.code
    }).subscribe(responseValues => {
      console.log(responseValues);
    },
    failureMsg => {
      console.log(failureMsg);
      this.failure = failureMsg;
    });
    //form.reset();
  }

  clearError(){
    this.failure = null;
  }

}
