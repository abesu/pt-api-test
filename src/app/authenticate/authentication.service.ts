import { User, AuthMessage} from './user.model';
import { SigninData } from './signin/signin-data.model';
import { SignupData } from './signup/signup-data.model';
import { Subject, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { ValidateData } from './validate/validate-data.model';

interface AuthentificationResponse {
    access_token?: string;
    expires_in?: string;
    adresse?: string;
    id_default_C?: string;
    jours_feries?: string;
    last_name?: string;
    location?: string;
    name?: string;
    pays?: string;
    refresh_token?: string;
    sexe?: string;
    token_type?: string;
    url_photo?: string;
    user_id?: number;
    user_type?: string;
    message?: string;
}

@Injectable()
export class AuthenticationService {
    user = new Subject<User>();
    authMessage = new Subject<AuthMessage>();

    constructor(private router: Router, private http: HttpClient) {

    }
    signIn(signinData: SigninData) {
       return this.http.post<AuthentificationResponse>('https://rec.backend.prettytime.co/public/api/login', {
            email: signinData.email,
            password: signinData.password,
        }).pipe(catchError(resError => {
            let failureMsg = 'Something is not right';
            if (!resError.error || !resError.error.error) {
                return throwError(failureMsg);
            } else if (resError.error.error === 'account_not_activated') {
                failureMsg = resError.error.message;
                localStorage.setItem('ptuserActivation', signinData.email);
                this.router.navigate(['/validate']);
            } else {
                failureMsg = resError.error.message;
            } 
            return throwError(failureMsg);
        }), tap(response => {
            const tExpDate = new Date(new Date().getTime() + + response.expires_in * 1000);
            const user = new User(response.user_id, response.access_token, tExpDate,);
            this.user.next(user);
            localStorage.setItem('ptlocalUserData', response.access_token);
            console.log('redirecting user');
            this.router.navigate(['/appointments']);
          } )); 
        }

    validateAccount(validData: ValidateData){
        let  validemail = localStorage.getItem('ptuserActivation');
        return this.http.post<AuthentificationResponse>('https://rec.backend.prettytime.co/public/api/activercompte', {
            email: validemail,
            code: validData.code,
        }).pipe(catchError(resError => {
            let failureMsg = 'Something is not right';
            if (!resError.error || !resError.error.error) {
                return throwError(resError);
            } else {
                failureMsg = resError.error.message;
            } 
            return throwError(failureMsg);
        }), tap(response => {
            const authMessage = new AuthMessage(response.message);
            this.authMessage.next(authMessage);
            console.log(response.message);
            localStorage.removeItem('ptuserActivation');
            this.router.navigate(['/signin']);
          } ));
    }    
        

    signUp(signupData: SignupData) {
        return this.http.post<AuthentificationResponse>('https://rec.backend.prettytime.co/public/api/register', {
            email: signupData.email,
            password: signupData.password,
            name: signupData.name,
            //last_name: signupData.last_name,
            num_telephone: signupData.num_telephone,
            type_user: signupData.type_user,
            password_confirmation: signupData.password_confirmation,
            cgu: signupData.cgu,
            pays: signupData.pays,
        }).pipe(catchError(resError => {
            let failureMsg = 'Something is not right';
            let errorArray = [];
            if (!resError.error || !resError.error.error || !resError.error.errors) {
                return throwError(resError);
            } /*else if (resError.error.errors.email || resError.error.errors.num_telephone) {
                errorArray = resError.error.errors;
                console.log(errorArray);
                //return throwError
            } */else {
                failureMsg = resError.error.message;
            } 
            return throwError(failureMsg);
        }), tap(response => {
            const authMessage = new AuthMessage(response.message);
            this.authMessage.next(authMessage);
            console.log(response.message);
            localStorage.setItem('ptuserActivation', signupData.email);
            this.router.navigate(['/validate']);
          } ));
    }

    signOut() {
        this.user.next(null);
        this.router.navigate(['/signin']);
        localStorage.removeItem('ptlocalUserData');
    }

    getUser() {
        return { ...this.user };
    }

    userAuthenticated() {
        return this.user != null;
    }
}