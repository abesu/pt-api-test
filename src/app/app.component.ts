import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from './authenticate/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'PT API test';
  isAuthenticated: boolean;
  private userSub: Subscription;

  constructor(private authenticationService: AuthenticationService) {
  }

  /*ngOnInit() {
    this.authSub = this.authenticationService.authenticated.subscribe(auth => {
      this.isAuthenticated = auth;

    })
  }

  

  ngOnDestroy() {
    this.authSub.unsubscribe;
  }*/

  ngOnInit() {
   this.userSub = this.authenticationService.user.subscribe(user => {
     this.isAuthenticated = !user ? false : true;
   });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe;

  }
  
  onSignout() {
    this.authenticationService.signOut();
  }

}
